<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/2.3.2/css/bootstrap-responsive.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="style.css"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
</head>

<script>
//cuando se hace una solicitud para modificar un mérito, se recoge aquí 
function corregir(tabla,id){
    $.ajax({  
         url:"library/tablaadmin/corregi2.php",  
         method:"POST",  
         data : { "id" : id, "tabla": tabla }, 
         success:function(data){  
               $('#modal_comming').html(data);
               $('#dataModal').modal('show');
         }  
    });  
} 

</script>
 
<?php
//muestra los méritos de lso apartados que no tengan subapartados.
//la construcción de la tabla es distinta a si los tuviera.
function mostrar_pendientes($tabla){
    $primera=1;
    global $contador, $pre, $tabla_total, $apartado;
    $letra=$pre[$contador];
    
    $resultado=getmerito_busqueda($tabla,'0');
    while ($lineaBD = $resultado->fetch_assoc()) {
        if ($primera==1){
 echo"
    <div id=\"collapse".$apartado.$letra."\" class=\"row-fluid collapse out\">
			<div>
               <div class=\"table\">
                  <table id=\"tablenosub\" class=\"table table-bordred table-stripedt\">
                  <thead>
                    <th style=\"text-align:left;\">Titulo</th>
                    <th style=\"text-align:left;\">Estado</th>
                  </thead>";
        $primera=0;
        }
        echo "
        <tbody>
            <tr>
							<td style=\"text-align:left;\">
								 " . $lineaBD['titulo']."
							</td>
							<td style=\"text-align:left;\">";
							switch ($lineaBD['estado']) {
								case 0:
									echo"<i class=\"material-icons\" style=\"font-size:24px;color:grey\">fiber_manual_record</i>";
									break;
								case 1:
									echo"<i class=\"material-icons\" style=\"font-size:24px;color:green\">fiber_manual_record</i>";
									break;
								case 2:
									echo"<i class=\"material-icons\" style=\"font-size:24px;color:yellow\">fiber_manual_record</i>";
									break;
								case 3:
									echo"<i class=\"material-icons\" style=\"font-size:24px;color:red\">fiber_manual_record</i>";
									break;
								default:
									echo 'Su usuario es incorrecto, intente nuevamente.';
									break;
							}
							
							

              echo 	$lineaBD['infoestado']
              ."</td>";
              if(($tabla_total!="da")&&($tabla_total!="db")&&($tabla_total!="dc")&&($tabla_total!="dd")&&($tabla_total!="dj")&&($tabla_total!="dl"))
              echo"
            <td style=\"text-align:right;\">
                              <button onclick=\"corregir('$tabla_total','$lineaBD[id]')\" style=\"color:blue; background-color: #ffffff; border: #ffffff;\"data-toggle=\"modal\" name='id'; value=\"".$tabla_total."\"> <i class=\"material-icons\">arrow_forward</i></button>
            </td>";

                echo"
              </form>
                            </td>						 
						</tr>
                        ";
        }
    echo'</tbody>
    </table>
    </div>
    </div>
    </div>';
    }
    //muestra los méritos de los apartados que tengan subapartados
  function mostrar_sub_pendientes($fichero, $num_linea){

        global $contador, $pre, $pret, $tabla, $tabla_total, $apartado;
        
        $letra=$pre[$contador];
        $letrat=$pret[$contador];
        $linea=$fichero[++$num_linea];
        $primera=1;
        
        while ($linea[0]=="."){
            if ($primera!=1)
                echo"<div id=\"collapse".$apartado.$letra."\" class=\"row-fluid collapse in\">";  
            if ($primera=1){
                echo"<div id=\"collapse".$apartado.$letra."\" class=\"row-fluid collapse out\">"; 
                $primera=0;}    
          $userid=$_SESSION["idbusqueda"];
			echo"
			<div>
               <div class=\"table\">
                  <table id=\"tablesub\" class=\"table table-bordred table-stripedt\">";   
                  for ($subtipo = 1,$linea=$fichero[$num_linea]; $linea[0]=='.'; $subtipo++,$linea=$fichero[++$num_linea] ){

                        $resultado=getmerito_busqueda($tabla_total,$subtipo);
                        
                        echo"<thead>
                        <th style=\"text-align:left;\">".$letrat.$subtipo.$linea."</th>
                        <th style=\"text-align:left;\">Estado</th>
                     </thead>";
                        echo "<tbody>";
                            while ($lineaBD = $resultado->fetch_assoc()) {
                                if ($lineaBD['subtipo']==$subtipo) {
                                    echo"
					 	<tr>
							<td style=\"text-align:left;\">
								 " . $lineaBD['titulo']."
							</td>
							<td style=\"text-align:left;\">";
							switch ($lineaBD['estado']) {
								case 0:
									echo"<i class=\"material-icons\" style=\"font-size:24px;color:grey\">fiber_manual_record</i>";
									break;
								case 1:
									echo"<i class=\"material-icons\" style=\"font-size:24px;color:green\">fiber_manual_record</i>";
									break;
								case 2:
									echo"<i class=\"material-icons\" style=\"font-size:24px;color:yellow\">fiber_manual_record</i>";
									break;
								case 3:
									echo"<i class=\"material-icons\" style=\"font-size:24px;color:red\">fiber_manual_record</i>";
                  break;
                  
								default:
									echo 'Su usuario es incorrecto, intente nuevamente.';
									break;
							}
							
              echo 	$lineaBD['infoestado']
              ."</td>";
              if(($tabla_total!="da")&&($tabla_total!="db")&&($tabla_total!="dc")&&($tabla_total!="dd")&&($tabla_total!="dj")&&($tabla_total!="dl"))
              echo"
            <td style=\"text-align:right;\">
                              <button onclick=\"corregir('$tabla_total','$lineaBD[id]')\" style=\"color:blue; background-color: #ffffff; border: #ffffff;\"data-toggle=\"modal\" name='id'; value=\"".$tabla_total."\"> <i class=\"material-icons\">arrow_forward</i></button>
            </td>";
            
           echo"
						</tr>
                        ";
                                }
                            }
                            echo "
                            </tbody>
                            ";
                        }
                        
                  echo"
                  </table>       
                  </div>
			      </div>";
                    }
            echo"</div>
			      </div>";
            return ($num_linea);
}
?>


<header id="main-header">
<div class="row-fluid">
<div class="col-lg-5">
<IMG SRC="/potencial/images/logoULPGC.jpg"  width="300px">
</div>
<br><br><br><br><br>
<!-- boton que muestra una ventana modal con la informacion del usuario que hemos buscado
     Se nos permite modificarla -->  
<div class="col-lg-6" id="titulousuario">   
   <button id="nuevousuario" style="background-color: #013068" data-toggle="modal" data-target="#verusuario"> <i class="material-icons">&#xE8B6;</i> Ver Usuario
</div>
<br><br>
</div>
</div>
</div>

</header>



<?php
$dni = htmlspecialchars($_POST['dni']);
include 'library/libreria.php';
$_SESSION["idbusqueda"]=buscarUsuario($dni);
////////////////////////////////////////

$pre=["a","b","c","d","e","ff","g","h","i","j","k","l","m","nn","nnn","o","fin"];
$pret=["A","B","C","D","E","F","G","H","I","J","K","L","M","N","Ñ","O","fin"];
$num_linea=0;
$contador=0;
$apartado="d";

$fichero = file('txt/titulos.txt');
if($fichero!=NULL){
    $fichero[0] = substr($fichero[0], 3);
    echo"
    <div id=\"resultadooo\"></div>
    <div class=\"container-fluid\" >
    <h1 id=\"titulo\">Planificaci&oacuten Acad&eacutemica</h1>";
    
    



    echo"
    <div class=\"col-sm-12\" align=\"center\" id=\"apartado\">DOCENCIA</div>
    <br><br>
		<div class=\"col-sm-10\" align=\"center\" id=\"concepto\">CONCEPTO</div>
        <div class=\"col-sm-1\" id=\"concepto\"></div>
        <div class=\"col-sm-1\" id=\"concepto\"></div>
    </div>";
    for ($linea=$fichero[$num_linea];$linea[0]!='/';$linea=$fichero[$num_linea]){
        $tabla_total=$apartado.$pre[$contador];
        switch ($linea['0']) {
          //si encontramos un guion, significa que es un apartado con subapartados
            case '-':
            
            $linea = substr($linea, 1);
            
            echo "<div class=\"row-fluid\" cursor: hand; cursor: pointer; id=\"concepto\">
                    <div class=\"accordion-toggle\" data-toggle=\"collapse\"
                        data-target=\"#collapse".$apartado.$pre[$contador]."\">
                        <div class=\"col-sm-10\"  id=\"aux\" >".$pret[$contador]."-".$linea."</div>
                        <div class=\"col-sm-1\"></div> </div>
                        <div class=\"col-sm-1\">";
                    echo"
                    </div>
                </div>";
            $num_linea=mostrar_sub_pendientes($fichero, $num_linea);
            $contador++;
            break;
//si encontramos un menor que, cambiamos de bloque
            case '<':
            
            if ($apartado=="i"){
                $apartado="g";
                echo"
                <br><br>
                <div class=\"col-sm-12\" align=\"center\" id=\"apartado\">GESTIÓN</div>
                <br><br><br><br><br><br><br>
                <div class=\"col-sm-10\" align=\"center\" id=\"concepto\">CONCEPTO</div>
                <div class=\"col-sm-1\" id=\"concepto\"></div>
                <div class=\"col-sm-1\" id=\"concepto\"></div>
                </div>";
            }
            if ($apartado=="d"){
                $apartado="i";
                echo"
                <br><br>
                <div class=\"col-sm-12\" align=\"center\" id=\"apartado\">INVESTIGACIÓN</div>
                <br><br><br>
                <div class=\"col-sm-10\" align=\"center\" id=\"concepto\">CONCEPTO</div>
                <div class=\"col-sm-1\" id=\"concepto\"></div>
                <div class=\"col-sm-1\" id=\"concepto\"></div>
            </div>";
            }
            $num_linea++;
            $contador=0;
              break;
//no se hace uso de este caracter para discriminar en el txt.
            case '#':

            $num_linea++;
            break;

//si no es ninguno de los casos anteriores, estamos en un apartado sin subapartados            
            default;
                $linea = '-'.$linea;
                echo   "<div class=\"row-fluid\"  id=\"concepto\">
                        <div class=\"accordion-toggle\" data-toggle=\"collapse\"data-target=\"#collapse".$apartado.$pre[$contador]."\">
                            <div class=\"col-sm-10\"  id=\"aux\">".$pret[$contador].$linea."</div>
                            <div class=\"col-sm-1\"></div> </div>
                            <div class=\"col-sm-1\">";
                        echo"
                        </div>
                    </div>";
                mostrar_pendientes($tabla_total);
                $num_linea++;
                $contador++;
                break;
        }
    }
}
?>
</div>
</html>

<div id="dataModal" class="modal fade" data-backdrop="static">  
  <div class="modal-dialog">  
    <div class="modal-content">  
      <div class="modal-header">    
      <button id="cerrar" type="button" class="btn btn-default" data-dismiss="modal">X</button>
        <h4 class="modal-title">Employee Details <button type="button" class="btn btn-default" data-dismiss="modal" style="Position:relative; left:60%">Close</button></h4>  
        
      </div>  
      <div class="modal-body" id="modal_comming">  
      </div>  
      <div class="modal-footer">  
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  
      </div>  
    </div>  
  </div>  
</div>  

<?php 

//MODALES DE AQUI EN ADELANTE

    //AÑADIR

    $mysqli=conectar();
    $id=$_SESSION['idbusqueda'];
    $query = "SELECT * FROM user_profile WHERE id=$id";
    $resultado = $mysqli->query($query);
    $lineaBD = $resultado->fetch_assoc();

echo"
<div class=\"modal fade\" id=\"verusuario\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"new\" aria-hidden=\"true\">
<div class=\"modal-dialog\">
   <div class=\"modal-content\">
      <div class=\"modal-header\">
      <button id=\"cerrar\" type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">X</button>
         <h4 class=\"modal-title custom_align\" id=\"Heading\">MODIFICAR USUARIO</h4>
      </div>
      <div class=\"modal-body\">
          <div class=\"panel-body\">
     <form id=\"insertaid\" name=\"newid\" action=\"library/tablaadmin/modificarusuario.php\" method=\"post\" class=\"form-horizontal\" autocomplete=\"on\">

           <label for=\"nombre\">Nombre</label></p>
           <input type=\"text\" class=\"form-control\" id=\"nombre\" name=\"nombre\" value = \"".$lineaBD['nombre']."\">

           <label for=\"orgfin\">Apellidos</label></p>
           <input type=\"text\" class=\"form-control\" id=\"apellido\" name=\"apellido\" value = \"".$lineaBD['apellido']."\">

           <label for=\"fechapub\">Teléfono</label>
           <input class=\"form-control\" id=\"phone\" name=\"phone\" type=\"text\" value = \"".$lineaBD['phone']."\">

           <label for=\"participacion\">Correo electrónico</label>
           <input class=\"form-control\" id=\"email\" name=\"email\" type=\"text\" value = \"".$lineaBD['email']."\">

           <label for=\"regional\">Fecha de nacimiento</label>
           <input class=\"form-control\" id=\"birthdate\" name=\"birthdate\" type=\"date\" value = \"".$lineaBD['birthdate']."\">

           <label for=\"rol\">Cargo del usuario</label></p>
           <select name=\"subtipo\">    
              <option value=\"1\" "; if($lineaBD['id_rol']==1) echo" selected"; echo">Personal docente</option>
              <option value=\"2\" "; if($lineaBD['id_rol']==2) echo" selected"; echo">Evaluador</option>
           </select>
           <br>
           <label for=\"rol\">Categoria profesional</label></p>
           <select name=\"categoria\">    
           <option value=\"1\" "; if($lineaBD['categoria']==1) echo" selected"; echo">Catedrático de Universidad</option>
           <option value=\"2\" "; if($lineaBD['categoria']==2) echo" selected"; echo">Catedrático de Universidad Vinculado</option>
           <option value=\"3\" "; if($lineaBD['categoria']==3) echo" selected"; echo">Titular de Universidad</option>
           <option value=\"4\" "; if($lineaBD['categoria']==4) echo" selected"; echo">Catedrático de Escuela Universitaria</option>
           <option value=\"5\" "; if($lineaBD['categoria']==5) echo" selected"; echo">Titular de Escuela Vinculado</option>
           <option value=\"6\" "; if($lineaBD['categoria']==6) echo" selected"; echo">Titular de Escuela Universitaria</option>
           <option value=\"7\" "; if($lineaBD['categoria']==7) echo" selected"; echo">Profesor Contratado Doctor</option>
           <option value=\"8\" "; if($lineaBD['categoria']==8) echo" selected"; echo">Profesor Ayudante Doctor</option>
           <option value=\"9\" "; if($lineaBD['categoria']==9) echo" selected"; echo">Profesor Colaborador</option>
           <option value=\"10\" "; if($lineaBD['categoria']==10) echo" selected"; echo">Ayudante</option>
           <option value=\"11\" "; if($lineaBD['categoria']==11) echo" selected"; echo">Asociado a Tiempo Parcial 3 horas</option>
           <option value=\"12\" "; if($lineaBD['categoria']==12) echo" selected"; echo">Asociado a Tiempo Parcial 4 horas</option>
           <option value=\"13\" "; if($lineaBD['categoria']==13) echo" selected"; echo">Asociado a Tiempo Parcial 5 horas</option>
           <option value=\"14\" "; if($lineaBD['categoria']==14) echo" selected"; echo">Asociado a Tiempo Parcial 6 horas</option>
           <option value=\"15\" "; if($lineaBD['categoria']==15) echo" selected"; echo">Asociado Ciencias de la Salud 3 horas</option>
           <option value=\"16\" "; if($lineaBD['categoria']==16) echo" selected"; echo">Asociado Ciencias de la Salud 4 horas</option>
           <option value=\"17\" "; if($lineaBD['categoria']==17) echo" selected"; echo">Asociado Ciencias de la Salud 5 horas</option>
           <option value=\"18\" "; if($lineaBD['categoria']==18) echo" selected"; echo">Asociado Ciencias de la Salud 6 horas</option>
         </select>



           <br>

           <input type=\"hidden\" name=\"usuario\" value=\"".$id."\" />

           <div class=\"modal-footer\">
           <input type=\"submit\" name=\"nuevousuario\" id=\"nuevousuario\" class=\"btn btn-success\" style=\"width:100%;\" value=\"Guardar\"/>
         </div>
       </form>
     </div>
   </div>
   <!-- /.modal-content --> 
</div>
<!-- /.modal-dialog --> 
</div></div>
     </div></div>";

?>
	<!-- Le javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="http://getbootstrap.com/2.3.2/assets/js/jquery.js"></script>

	<script src="http://getbootstrap.com/2.3.2/assets/js/holder/holder.js"></script>
	<script src="http://getbootstrap.com/2.3.2/assets/js/google-code-prettify/prettify.js"></script>
	<script src="http://getbootstrap.com/2.3.2/assets/js/application.js"></script>
  
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>