<?php
//agregamos la libreria de funciones
include 'library/libreria.php';
?>

<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/2.3.2/css/bootstrap-responsive.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="style.css"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<title>Planificación Académica</title>
</head>



<script>
//funcion para cerrar la sesion actual
function cerrarSesion(){

      if(confirm('¿Desea cerrar la sesion?'))
      {

        $.ajax({
            // la URL para la petición
            url : 'library/cerrarsesion.php',
      
            // código a ejecutar si la petición es satisfactoria:
            //    volver a index.php
            success : function() {
              location.href="index.php";
            }
        });

      }
      else
      {
        return false;
      }
 
    }


//cuando se hace una solicitud para modificar un mérito, se recoge aquí
function corregir(tabla,id){
    $.ajax({ 
         url:"library/tablaadmin/corregi2.php",  
         method:"POST",  
         data : { "id" : id, "tabla": tabla }, 
         success:function(data){  
               $('#modal_comming').html(data);
               $('#dataModal').modal('show');
         }  
    });  
} 
</script>


<?php
//muestra los méritos de lso apartados que no tengan subapartados.
//la construcción de la tabla es distinta a si los tuviera.
function mostrar_pendientes($tabla){
    $primera=1;
    global $contador, $pre, $tabla_total, $apartado;
    $letra=$pre[$contador];
    
    if(isset($_POST['submit']))
      $resultado=getmerito_admin($tabla,'0', $_POST['estado']);
    else
      $resultado=getmerito_admin($tabla,'0','4');

    while ($lineaBD = $resultado->fetch_assoc()) {
        if ($primera==1){
 echo"
    <div id=\"collapse".$apartado.$letra."\" class=\"row-fluid collapse out\">
			<div>
               <div class=\"table\">
                  <table id=\"tablenosub\" class=\"table table-bordred table-stripedt\">
                  <thead>
                    <th style=\"text-align:left;\">Titulo</th>
                    <th style=\"text-align:left;\">Estado</th>
                  </thead>";
        $primera=0;
        }
        echo "
        <tbody>
            <tr>
							<td style=\"text-align:left;\">
								 " . $lineaBD['titulo']."
							</td>
							<td style=\"text-align:left;\">";
							switch ($lineaBD['estado']) {
								case 0:
									echo"<i class=\"material-icons\" style=\"font-size:24px;color:grey\">fiber_manual_record</i>";
									break;
								case 1:
									echo"<i class=\"material-icons\" style=\"font-size:24px;color:green\">fiber_manual_record</i>";
									break;
								case 2:
									echo"<i class=\"material-icons\" style=\"font-size:24px;color:yellow\">fiber_manual_record</i>";
									break;
								case 3:
									echo"<i class=\"material-icons\" style=\"font-size:24px;color:red\">fiber_manual_record</i>";
									break;
								default:
									echo 'Su usuario es incorrecto, intente nuevamente.';
									break;
							}
							
							

              echo 	$lineaBD['infoestado']
              ."</td>";
              if(($tabla_total!="da")&&($tabla_total!="db")&&($tabla_total!="dc")&&($tabla_total!="dd")&&($tabla_total!="dj")&&($tabla_total!="dl"))
              echo"
            <td style=\"text-align:right;\">
                              <button onclick=\"corregir('$tabla_total','$lineaBD[id]')\" style=\"color:blue; background-color: #ffffff; border: #ffffff;\"data-toggle=\"modal\" name='id'; value=\"".$tabla_total."\"> <i class=\"material-icons\">arrow_forward</i></button>
            </td>";

                echo"
              </form>
                            </td>						 
						</tr>
                        ";
        }
    echo'</tbody>
    </table>
    </div>
    </div>
    </div>';
    }
//muestra los méritos de los apartados que tengan subapartados
  function mostrar_sub_pendientes($fichero, $num_linea){

        global $contador, $pre, $pret, $tabla, $tabla_total, $apartado;
        
        $letra=$pre[$contador];
        $letrat=$pret[$contador];
        $linea=$fichero[++$num_linea];
        $primera=1;
        
        while ($linea[0]=="."){
            if ($primera!=1)
                echo"<div id=\"collapse".$apartado.$letra."\" class=\"row-fluid collapse in\">";  
            if ($primera=1){
                echo"<div id=\"collapse".$apartado.$letra."\" class=\"row-fluid collapse out\">"; 
                $primera=0;}    
			echo"
			<div>
               <div class=\"table\">
                  <table id=\"tablesub\" class=\"table table-bordred table-stripedt\">";   
                  for ($subtipo = 1,$linea=$fichero[$num_linea]; $linea[0]=='.'; $subtipo++,$linea=$fichero[++$num_linea] ){
                    if(isset($_POST['submit'])){
                      $estado=$_POST['estado'];
                      $resultado=getmerito_admin($tabla_total,'0',$estado);
                    }
                    else
                      $resultado=getmerito_admin($tabla_total,'0','4');
                        
                        echo"<thead>
                        <th style=\"text-align:left;\">".$letrat.$subtipo.$linea."</th>
                        <th style=\"text-align:left;\">Estado</th>
                     </thead>";
                        echo "<tbody>";
                            while ($lineaBD = $resultado->fetch_assoc()) {
                                if ($lineaBD['subtipo']==$subtipo) {
                                    echo"
					 	<tr>
							<td style=\"text-align:left;\">
								 " . $lineaBD['titulo']."
							</td>
							<td style=\"text-align:left;\">";
							switch ($lineaBD['estado']) {
								case 0:
									echo"<i class=\"material-icons\" style=\"font-size:24px;color:grey\">fiber_manual_record</i>";
									break;
								case 1:
									echo"<i class=\"material-icons\" style=\"font-size:24px;color:green\">fiber_manual_record</i>";
									break;
								case 2:
									echo"<i class=\"material-icons\" style=\"font-size:24px;color:yellow\">fiber_manual_record</i>";
									break;
								case 3:
									echo"<i class=\"material-icons\" style=\"font-size:24px;color:red\">fiber_manual_record</i>";
                  break;
                  
								default:
									echo 'Su usuario es incorrecto, intente nuevamente.';
									break;
							}
							
              echo 	$lineaBD['infoestado']
              ."</td>";
              if(($tabla_total!="da")&&($tabla_total!="db")&&($tabla_total!="dc")&&($tabla_total!="dd")&&($tabla_total!="dj")&&($tabla_total!="dl"))
              echo"
            <td style=\"text-align:right;\">
                              <button onclick=\"corregir('$tabla_total','$lineaBD[id]')\" style=\"color:blue; background-color: #ffffff; border: #ffffff;\"data-toggle=\"modal\" name='id'; value=\"".$tabla_total."\"> <i class=\"material-icons\">arrow_forward</i></button>
            </td>";
            
           echo"
						</tr>
                        ";
                                }
                            }
                            echo "
                            </tbody>
                            ";
                        }
                        
                  echo"
                  </table>       
                  </div>
			      </div>";
                    }
            echo"</div>
			      </div>";
            return ($num_linea);
}
?>


<header id="main-header">
<div class="row-fluid">
<div class="col-lg-3">
  <IMG SRC="/potencial/images/logoULPGC.jpg"  width="300px">
</div>
<br><br><br><br>

<?php
session_start();
if(!isset($_SESSION['id'])||($_SESSION['rol']!=2))
  goback("index.php");
?>

<div class="col-lg-3"> 
<!-- Formulario para buscar usuario por dni -->  
<form id="buscausuario" name="buscausuario" action="tablaadminbusqueda.php" method="post" class="form-horizontal" autocomplete="on">
  <input type="text" name="dni" id ="dni" placeholder="Introduzca dni de usuario">
  <input type="submit" value="Buscar">
</form>
</div>
<!-- botón que llama a una ventana emergente para crear un nuevo usuario -->
<div class="col-lg-4" id="titulousuario">
   <button id="nuevousuario" style="background-color: #013068" data-toggle="modal" data-target="#usuarionuevo"> <i class="material-icons">person_add</i> Añadir Usuario
</div>
<div class="col-lg-2" id="titulousuario">
    ¡Hola<a href="/potencial/library/usuario.php">
    <?php
     /* if(!isset($_SESSION['id'])||($_SESSION['rol']!=2))
        echo '<script type="text/javascript"> 
               window.location="index.php"; 
              </script>';
      else*/
        echo " ".$_SESSION['nombre']."!";
    ?>
    </a>
    <br>
    <a href="#" onclick="cerrarSesion();">Cerrar sesión</a>
</div>

 

</div>
<br><br><br>
</div>
</div>
</div>
</header>

Ver mérito según estado:
<form action="/Potencial/tablaadmin.php" method="post">
    <select name='estado'>
      <option value="0"<?php if(isset($_POST['estado'])&&$_POST['estado']==0) echo" selected";?>    >Pendientes de evaluación</option>
      <option value="1"<?php if(isset($_POST['estado'])&&$_POST['estado']==1) echo "selected";?>   >Aceptados</option>
      <option value="2"<?php if(isset($_POST['estado'])&&$_POST['estado']==2) echo "selected";?>    >Pendientes de subsanación por el usuario</option>
      <option value="3"<?php if(isset($_POST['estado'])&&$_POST['estado']==3) echo "selected";?>    >Rechazados</option>
      <option value="4"<?php if(isset($_POST['estado'])&&$_POST['estado']==4) echo "selected"; else echo "selected";?>    >Todos</option>
    </select>
    <button type="submit" name="submit" value="1" style="color:blue; background-color: #ffffff;border: #ffffff"><i class="material-icons">&#xE8B6;</i></button>
  </form>
  <?php

////////////////////////////////////////
$pre=["a","b","c","d","e","ff","g","h","i","j","k","l","m","nn","nnn","o","p","q","r","s"];
$pret=["A","B","C","D","E","F","G","H","I","J","K","L","M","N","Ñ","O","P","Q","R","S"];
$num_linea=0;
$contador=0;
$apartado="d";

$fichero = file('txt/titulos.txt');
if($fichero!=NULL){
    $fichero[0] = substr($fichero[0], 3);
    echo"
    <div id=\"resultadooo\"></div>
    <div class=\"container-fluid\" >
    <h1 id=\"titulo\">Planificaci&oacuten Acad&eacutemica</h1>";
    
    



    echo"
    <div class=\"col-sm-12\" align=\"center\" id=\"apartado\">ACTIVIDADES DE DOCENCIA</div>
    <br><br>
		<div class=\"col-sm-12\" align=\"center\" id=\"concepto\">CONCEPTO</div>
    </div>";
    for ($linea=$fichero[$num_linea];$linea[0]!='/';$linea=$fichero[$num_linea]){
        $tabla_total=$apartado.$pre[$contador];
        switch ($linea['0']) {
          //si encontramos un guion, significa que es un apartado con subapartados
            case '-':
            $linea = substr($linea, 1);
            echo "<div class=\"row-fluid\"cursor: hand; cursor: pointer; id=\"concepto\">
                    <div class=\"accordion-toggle\" data-toggle=\"collapse\"
                        data-target=\"#collapse".$apartado.$pre[$contador]."\">
                        <div class=\"col-sm-12\"  id=\"aux\" >".$pret[$contador]."-".$linea."</div>";
                    echo"
                    </div>
                </div>";
            $num_linea=mostrar_sub_pendientes($fichero, $num_linea);
            $contador++;
            break;
            //si encontramos un menor que, cambiamos de bloque
            case '<':
                
                if ($apartado=="i"){
                    $apartado="g";
                    echo"
                    <br><br>
                    <div class=\"col-sm-12\" align=\"center\" id=\"apartado\">ACTIVIDADES DE GESTIÓN</div>
                    <br><br><br><br><br><br><br>
                    <div class=\"col-sm-12\" align=\"center\" id=\"concepto\">CONCEPTO</div>
                    </div>";
                }
                if ($apartado=="d"){
                    $apartado="i";
                    echo"
                    <br><br>
                    <div class=\"col-sm-12\" align=\"center\" id=\"apartado\">ACTIVIDADES DE INVESTIGACIÓN, INNOVACIÓN, TRANSFERENCIA
                    DEL CONOCIMIENTO, COOPERACION Y FORMACIÓN</div>
                    <br><br><br>
                    <div class=\"col-sm-12\" align=\"center\" id=\"concepto\">CONCEPTO</div>
                </div>";
                }
                $num_linea++;
                $contador=0;
              break;
            //no se hace uso de este caracter para discriminar en el txt.
            case '#':

            $num_linea++;
            break;

            //si no es ninguno de los casos anteriores, estamos en un apartado sin subapartados
            default;
                $linea = '-'.$linea;
                echo   "<div class=\"row-fluid\" id=\"concepto\">
                        <div class=\"accordion-toggle\" data-toggle=\"collapse\"data-target=\"#collapse".$apartado.$pre[$contador]."\">
                            <div class=\"col-sm-12\"  id=\"aux\">".$pret[$contador].$linea."</div>";
                        echo"
                        </div>
                    </div>";
                mostrar_pendientes($tabla_total);
                $num_linea++;
                $contador++;
                break;
        }
    }
}
?>
</div>
</html>
<!-- cuando vamos a corregir un mérito, la información recuperada se vuelca en esta ventana modal-->
<div class="modal fade" id="dataModal" tabindex="-1" role="dialog" aria-labelledby="new" aria-hidden="true"> 
  <div class="modal-dialog">  
    <div class="modal-content">  
      <div class="modal-header">    
      <button id="cerrar" type="button" class="btn btn-default" data-dismiss="modal">X</button>
        <h4 class="modal-title">CORREGIR MÉRITO </h4>  
        
      </div>  
      <div class="modal-body" id="modal_comming">  
      </div>  
    </div>  
  </div>  
</div>  

<!-- ventana modal para crear usuario nuevo -->
<div class="modal fade" id="usuarionuevo" tabindex="-1" role="dialog" aria-labelledby="new" aria-hidden="true">
          <div class="modal-dialog">
             <div class="modal-content">
                <div class="modal-header">
                <button id="cerrar" type="button" class="btn btn-default" data-dismiss="modal">X</button>
                   <h4 class="modal-title custom_align" id="Heading">NUEVO USUARIO</h4>
                </div>
                <div class="modal-body">
                    <div class="panel-body">
               <form id="insertaid" name="newid" action="library/tablaadmin/nuevousuario.php" method="post" class="form-horizontal" autocomplete="on">
 
                     <label for="nombre">Nombre</label></p>
                     <input type="text" class="form-control" id="nombre" name="nombre" required>

                     <label for="orgfin">Apellidos</label></p>
                     <input type="text" class="form-control" id="apellidos" name="apellidos" required>

                     <label for="fechapub">Teléfono</label>
                     <input class="form-control" id="telefono" name="telefono" type="text" required>

                     <label for="participacion">DNI</label>
                     <input class="form-control" id="dni" name="dni" type="text" required>

                     <label for="participacion">Correo electrónico</label>
                     <input class="form-control" id="correo" name="correo" type="text" required>

                     <label for="regional">Fecha de nacimiento</label>
                     <input class="form-control" id="fechaNacimiento" name="fechaNacimiento" type="date" required>

                     <label for="lugar">Contraseña</label>
                     <input class="form-control" id="pass" name="pass"type="text" required>
                      <br>
                     <label for="rol">Cargo del usuario</label></p>
                     <select name="rol">    
                       <option value="1" selected="selected">Profesor</option>
                       <option value="2">Evaluador</option>
                     </select>
                    <br>
                    <br>
                     <label for="rol">Categoria Profesional</label></p>
                     <select name="categoria">    
                       <option value="1" selected="selected">Catedrático de Universidad</option>
                       <option value="2">Catedrático de Universidad Vinculado</option>
                       <option value="3">Titular de Universidad</option>
                       <option value="4">Catedrático de Escuela Universitaria</option>
                       <option value="5">Titular de Escuela Vinculado</option>
                       <option value="6">Titular de Escuela Universitaria</option>
                       <option value="7">Profesor Contratado Doctor</option>
                       <option value="8">Profesor Ayudante Doctor</option>
                       <option value="9">Profesor Colaborador</option>
                       <option value="10">Ayudante</option>
                       <option value="11">Asociado a Tiempo Parcial 3 horas</option>
                       <option value="12">Asociado a Tiempo Parcial 4 horas</option>
                       <option value="13">Asociado a Tiempo Parcial 5 horas</option>
                       <option value="14">Asociado a Tiempo Parcial 6 horas</option>
                       <option value="15">Asociado Ciencias de la Salud 3 horas</option>
                       <option value="16">Asociado Ciencias de la Salud 4 horas</option>
                       <option value="17">Asociado Ciencias de la Salud 5 horas</option>
                       <option value="18">Asociado Ciencias de la Salud 6 horas</option>
                     </select>
                     <br>

                     <div class="modal-footer">
                     <input type="submit" name="nuevousuario" id="nuevousuario" class="btn btn-success" style="width:100%;" value="Guardar"/>
                   </div>
                 </form>
               </div>
             </div>
             <!-- /.modal-content --> 
          </div>
          <!-- /.modal-dialog --> 
       </div></div>
               </div></div>

	<!-- Le javascript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="http://getbootstrap.com/2.3.2/assets/js/jquery.js"></script>

	<script src="http://getbootstrap.com/2.3.2/assets/js/holder/holder.js"></script>
	<script src="http://getbootstrap.com/2.3.2/assets/js/google-code-prettify/prettify.js"></script>
	<script src="http://getbootstrap.com/2.3.2/assets/js/application.js"></script>
  
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>